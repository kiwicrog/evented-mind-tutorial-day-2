/*****************************************************************************/
/* TodosList: Event Handlers */
/*****************************************************************************/
Template.TodosList.events({
    todos: function() {
        return Todos.find({userId: Meteor.userId()}, {
            sort: {createdAt: -1}
        });
    },

    isDoneClass: function() {
        return this.isDone ? 'done' : '';
    }
});

/*****************************************************************************/
/* TodosList: Helpers */
/*****************************************************************************/
Template.TodosList.helpers({
});

/*****************************************************************************/
/* TodosList: Lifecycle Hooks */
/*****************************************************************************/
Template.TodosList.onCreated(function () {
});

Template.TodosList.onRendered(function () {
});

Template.TodosList.onDestroyed(function () {
});
